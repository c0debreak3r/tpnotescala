package fr.upem.partiel.part2.parser

import fr.upem.partiel.part2.model.Movie
import fr.upem.partiel.part2.model.Movie.Country._
import fr.upem.partiel.part2.model.Movie._
import play.api.libs.functional.syntax._
import play.api.libs.json._

object Parser {

  def toDirector: String => Option[Director] = (directorStr : String) => directorStr.split(" ") match {
    case Array(fn, ln) if(fn.length > 0 && ln.length > 0) => Some(Director(fn, ln))
    case _ => None
  }

  // TODO
  def toName: String => Title = ???

  def toCountry: String => Option[Country] = (countryStr : String) => countryStr match {
    case "FR" => Some(France)
    case "UK" => Some(England)
    case "GE" => Some(Germany)
    case "IT" => Some(Italy)
    case "US" => Some(UnitedStates)
    case _ => None
  }

  import fr.upem.partiel.part1.Part1.StringUtils._
  def toYear: String => Option[Year] = (yearStr : String) => yearStr.split("") match {
    case Array(fst, snd, trd, frth) => (fst.atoi, snd.atoi, trd.atoi, frth.atoi) match {
      case (Some(a), Some(b), Some(c), Some(d)) if(a == 1 || a == 2) => Some(Year(a * 1000 + b * 100 + c * 10 + d))
      case _ => None
    }
    case _ => None
  }

  def toViews: BigDecimal => Option[Views] = (views : BigDecimal) => if(views > BigDecimal(Long.MaxValue) || views <= 0) Some(Views(views.longValue)) else None

  implicit val directorReads = Reads[Director] {
    case JsString(value) => toDirector(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Director"))
    case _ => JsError("Not a valid type for Director")
  }

  implicit val nameReads = Reads[Title] {
    case JsString(value) => JsSuccess(toName(value))
    case _ => JsError("Not a valid type for Name")
  }

  implicit val countryReads = Reads[Country] {
    case JsString(value) => toCountry(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Country"))
    case _ => JsError("Not a valid type for Country")
  }

  implicit val yearReads = Reads[Year] {
    case JsString(value) => toYear(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Year"))
    case _ => JsError("Not a valid type for Year")
  }

  implicit val viewsReads = Reads[Views] {
    case JsNumber(value) => toViews(value).map(JsSuccess(_)).getOrElse(JsError("Not a valid Views"))
    case _ => JsError("Not a valid type for Views")
  }

  implicit val movieReads: Reads[Movie] = null
  /*
  implicit val movieReads: Reads[Movie] = (
    (__ \ "title").read[Title] and
      (__ \ "director").read[Director] and
      (__ \ "year").read[Year] and
      (__ \ "views").read[Views] and
      (__ \ "country").read[Country]
    ) (Movie.apply _)
    */

}
