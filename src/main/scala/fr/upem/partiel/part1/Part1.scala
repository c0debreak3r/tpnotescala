package fr.upem.partiel.part1

import java.time.Instant
import java.time.temporal.ChronoUnit.YEARS


// Part 1 (10pts)
object Part1 {

  // 1.1 Apply 'mul2' using pattern matching to the given integer (.5pts)
  def mul2(i: Int): Int = i * 2

  def applyMul2WithPatternMatching(i: Option[Int]): Option[Int] = i match {
    case Some(value) => Some(mul2(value))
    case _ => None
  }

  // 1.2 Apply 'mul2' WITHOUT using pattern matching to the given integer (.5pts)
  def applyMul2WithoutPatternMatching(i: Option[Int]): Option[Int] = if (i.isDefined) Some(mul2(i.get)) else None

  // 1.3 Refactor the following code using pattern matching (1pts)
  sealed trait Animal

  case object Cat extends Animal

  case object Bird extends Animal

  case class Dog(age: Int) extends Animal

  def formatAnimal(animal: Animal): String = animal match {
    case Cat => "It's a cat"
    case Bird => "It's a bird"
    case Dog(age) => s"It's a $age year old dog"
  }

  // 1.4 Find the index of the given element if any, use recursivity (1pts)
  def indexOf[A](l: List[A], a: A): Option[Int] = l match {
    case Nil => None
    case head :: tl => if (head == a) Some(0) else indexOf(tl, a) match {
      case Some(value) => Some(1 + value)
      case _ => None
    }
  }


  // 1.5 Throw away all errors (.5pts)
  case class Error(message: String)

  def keepValid[A](l: List[Either[Error, A]]): List[A] = l.filter(either => either.isRight).map(e => e.right.get)

  // 1.6 Aggregate values (.5pts)
  def aggregate[A](l: List[A], combine: (A, A) => A, empty: A): A = l match {
    case Nil => empty
    case head :: tl => combine(head, aggregate(tl, combine, empty))
  }

  // 1.7 Aggregate valid values (.5pts)
  def aggregateValid[A](l: List[Either[Error, A]], combine: (A, A) => A, empty: A): A = aggregate(keepValid(l), combine, empty)


  // 1.8 Create the Monoid typeclass and rewrite the above "aggregateValid" (.5pts)
  trait Monoid[A] {
    def combine(a: A, b: A): A

    def empty: A
  }


  def aggregateValidM[A](l: List[A])(implicit ev: Monoid[A]): A = l.foldLeft(ev.empty)(ev.combine)

  // 1.9 Implement the Monoid typeclass for Strings and give an example usage with aggregateValidM (.5pts)

  implicit val stringImpl = new Monoid[String] {
    override def combine(a: String, b: String): String = a + b

    override def empty: String = ""
  }

  def aggregateValidMExample: String = aggregateValidM(List("Hello", " ", "world", "!"))


  // 1.10 Refactor the following object oriented hierarchy with an ADT (1.5pts)
  sealed trait FinancialAsset {
    def computeEarnings: Double
  }

  sealed trait FlatRateAsset extends FinancialAsset {
    def rate: Double

    def amount: Double

    override def computeEarnings: Double = amount + (amount * rate)
  }

  case class LivretA(amount: Double) extends FlatRateAsset {
    override def rate: Double = 0.75
  }

  case class Pel(amount: Double, creation: Instant) extends FlatRateAsset {
    override def rate: Double = 1.5

    override def computeEarnings: Double =
      if (Instant.now().minus(4, YEARS).isAfter(creation))
        super.computeEarnings + 1525
      else
        super.computeEarnings
  }

  case class CarSale(amount: Int, horsePower: Int) extends FinancialAsset {
    override def computeEarnings: Double = amount - (500 * horsePower)
  }

  // 1.11 Extract the "computeEarnings" logic of the above hierarchy
  // into an "Earnings" typeclass and create the adequate instances (1.5pts)

  // 1.12 Rewrite the following function with your typeclass (.5pts)
  def computeTotalEarnings(assets: List[FinancialAsset]): Double =
    assets.map(_.computeEarnings).sum


  // 1.13 Enrich the "String" type with an "atoi" extension method that parses the
  // given String to an Int IF possible (1pts)
  object StringUtils {
    implicit class StringUtils(val string: String) {
      def atoi: Option[Int] = {
        try {
          Some(string.toInt)
        } catch {
          case e: Exception => None
        }
      }
    }
  }
}
