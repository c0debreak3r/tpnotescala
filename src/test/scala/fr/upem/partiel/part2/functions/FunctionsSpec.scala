package fr.upem.partiel.part2.functions

import fr.upem.partiel.part2.generators.Generators.{directorGen, movieGen, myMovieGen, tupleGen}
import fr.upem.partiel.part2.model.Movie
import fr.upem.partiel.part2.model.Movie.{Country, Director}
import org.scalacheck.Gen.{alphaStr, nonEmptyListOf}
import org.scalactic.anyvals.PosInt
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FlatSpec, Matchers}

class FunctionsSpec extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks {

  implicit override val generatorDrivenConfig = PropertyCheckConfig(minSuccessful = PosInt(50), minSize = 10, maxSize = 20)

  "Functions" should "get director names" in {

    forAll(nonEmptyListOf(tupleGen[String](alphaStr))) { names =>
      val expected = names.map { case (fn, ln) => s"$fn $ln" }
      val movies = names.map { case (fn, ln) => Movie.director(fn, ln) }.map(d => Movie.movie(Movie.title(""), d, Movie.year(1987), Movie.views(1L), Country.Italy))
      Functions.getDirectorNames(movies) should be(expected)
    }
  }

  it should "filter by views" in {

    forAll(nonEmptyListOf(movieGen(10)), nonEmptyListOf(movieGen(100))) { (flops, tops) =>
      val movies = flops ::: tops
      Functions.viewMoreThan(50)(movies) should contain theSameElementsAs tops
    }
  }

  it should "group by director" in {

    (directorGen.sample, directorGen.sample) match {
      case (Some(d1), Some(d2)) => {
        (myMovieGen(d1).sample, myMovieGen(d2).sample) match {
          case (Some(m1), Some(m2)) => {
              val map  = Map(d1 -> List(m1), d2 -> List(m2))
              val movies = m1 :: m2 :: Nil
              Functions.byDirector(movies) should be (map)
          }
          case _ => ()
        }
      }
      case _ => ()
    }
  }
}

